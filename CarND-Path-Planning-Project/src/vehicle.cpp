#include <algorithm>
#include <iostream>
#include <cmath>
#include <map>
#include <string>
#include <iterator>
#include "vehicle.h"
#include "cost.h"

/**
 * Initializes Vehicle
 */

Vehicle::Vehicle(){}

// construct a Vehicle object and initialie it with the provided arguments 
Vehicle::Vehicle(lane lane_type, double x, double y, double s, double d, double yaw, double v, std::string state)
{
	this->lane_type = lane_type;
	this->x = x;
	this->y = y;
	this->s = s;
	this->d = d;
	this->yaw = yaw;
	this->v = v;
	this->state = state;
	too_close_ahead = false;
	too_close_behind = false;
	lane_change = false;
	cold_start = true;
	ref_vel = 0.0;
}

Vehicle::~Vehicle() {}

// state transition function 
trajectory Vehicle::choose_next_state(map<int, vector<Vehicle>> predictions) 
{
    /*     
    INPUT: A predictions map. This is a map using vehicle id as keys with predicted
        vehicle trajectories as values. A trajectory is a vector of Vehicle objects. The first
        item in the trajectory represents the vehicle at the current timestep. The second item in
        the trajectory represents the vehicle one timestep in the future.
    OUTPUT: The best (lowest cost) trajectory for the ego vehicle corresponding to the next ego vehicle state.

    Functions that will be useful:
    1. successor_states() - Uses the current state to return a vector of possible successor states for the finite 
       state machine.
	2. is_vehicle_ahead() - to check if there is any vehicle in front of the ego vehicle
    3. is_vehicle_behind() - to check if there is any vehicle behind the ego vehicle
    4. generate_trajectory(string state, map<int, vector<Vehicle>> predictions) - Returns a trajectory struct 
       representing a rough vehicle trajectory, given a state and predictions. Note that is_exist element is false 
	   if no possible trajectory exists for the state. 
    5. calculate_cost(Vehicle vehicle, map<int, vector<Vehicle>> predictions, vector<Vehicle> trajectory) - Included from 
       cost.cpp, computes the cost for a trajectory.
    */
    
	vector<std::string> states = successor_states();
	vector<double> costs;
	vector<std::string> final_states;
	vector<trajectory> final_trajectories;

	this->too_close_ahead = is_vehicle_ahead(predictions, this->lane_type);
	this->too_close_behind = is_vehicle_behind(predictions, this->lane_type);

	for (vector<std::string>::iterator it = states.begin(); it != states.end(); ++it)
	{
		trajectory traj = generate_trajectory(*it, predictions);
		if (traj.is_exist)
		{
			double cost = calculate_cost(*this, predictions, traj, this->too_close_ahead, this->too_close_behind, 
				this->lane_change);
			costs.push_back(cost);
			final_trajectories.push_back(traj);
		}
	}

	vector<double>::iterator best_cost = min_element(begin(costs), end(costs));
	int best_idx = distance(begin(costs), best_cost);
	this->state = states[best_idx];

	return final_trajectories[best_idx];
}

// Uses the current state to return a vector of possible successor states for the finite state machine
vector<std::string> Vehicle::successor_states()
{
    vector<std::string> states;
    states.push_back("KL");
	std::string state = this->state;

	if (state.compare("KL") == 0)
	{
		states.push_back("LCR");
		states.push_back("LCL");
	}
	else if (state.compare("LCL") == 0)
	{
		states.push_back("LCL");		
	}
	else if (state.compare("LCR") == 0)
	{
		states.push_back("LCR");		
	}

    return states;
}

// Returns a trajectory struct representing a rough vehicle trajectory, given a state and predictions. 
// Note that is_exist element is false if no possible trajectory exists for the state.
trajectory Vehicle::generate_trajectory(std::string state, map<int, vector<Vehicle>> predictions)
{
    /*
    Given a possible next state, generate the appropriate trajectory to realize the next state.
    */
    trajectory traj;

	if (state.compare("KL") == 0) 
    {
		traj = keep_lane_trajectory(predictions);
    } 
    else if ( (state.compare("LCL") == 0 && this->lane_type != left_lane) ||
		      (state.compare("LCR") == 0 && this->lane_type != right_lane) )
    {
		traj = lane_change_trajectory(state, predictions);
    } 	

    return traj;
}

// generates a keep lane trajectory
trajectory Vehicle::keep_lane_trajectory(map<int, vector<Vehicle>> predictions) 
{
	trajectory traj = { this->s, this->d, this->x, this->y, this->yaw, this->lane_type, this->lane_type, "KL", true};

	return traj;
}

// generates a lane change trajectory
trajectory Vehicle::lane_change_trajectory(std::string state, map<int, vector<Vehicle>> predictions)
{
	lane new_lane = static_cast<lane>(int(this->lane_type) + lane_direction[state]);

	trajectory traj = {};

	this->lane_change = is_lane_change_possible(predictions, new_lane);

	if (new_lane == invalid_lane || !this->lane_change)
	{
		traj.is_exist = false;

		return traj;
	}
	else
	{
		traj = { this->s, this->d, this->x, this->y, this->yaw, this->lane_type, new_lane, state, true };

		return traj;
	}
}

// returns a true if a vehicle is found behind the current vehicle within stipulated distance, false otherwise.
bool Vehicle::is_vehicle_behind(map<int, vector<Vehicle>> predictions, lane lane_type)
{

    Vehicle temp_vehicle;
    for (map<int, vector<Vehicle>>::iterator it = predictions.begin(); it != predictions.end(); ++it) 
    {
        temp_vehicle = it->second[0];
        if (temp_vehicle.lane_type == lane_type && temp_vehicle.s < this->s && (this->s - temp_vehicle.s) < stip_dist)
        {	
			return true;
        }
    }
    return false;
}

// returns a true if a vehicle is found ahead of the current vehicle within stipulated distance, false otherwise.
bool Vehicle::is_vehicle_ahead(map<int, vector<Vehicle>> predictions, lane lane_type) 
{
    Vehicle temp_vehicle;
    for (map<int, vector<Vehicle>>::iterator it = predictions.begin(); it != predictions.end(); ++it) 
    {
        temp_vehicle = it->second[1];
        if (temp_vehicle.lane_type == lane_type && temp_vehicle.s > this->s && (temp_vehicle.s - this->s) < stip_dist)
        {
			return true;
        }
    }
	return false;
}

// checks if lane change is possible
bool Vehicle::is_lane_change_possible(map<int, vector<Vehicle>> predictions, lane target_lane)
{
	if ( (this->lane_type == left_lane && target_lane == left_lane) ||
		 (this->lane_type == right_lane && target_lane == right_lane) ||
		 (this->lane_type == right_lane && target_lane == left_lane) ||
		 (this->lane_type == left_lane && target_lane == right_lane) )
	{
		return false;
	}	

	if (this->ref_vel < target_speed - 2 && this->ref_vel > target_speed + 2)
	{
		return false;
	}

	Vehicle temp_vehicle_0;
	Vehicle temp_vehicle_1;
	for (map<int, vector<Vehicle>>::iterator it = predictions.begin(); it != predictions.end(); ++it)
	{
		temp_vehicle_0 = it->second[0];
		if (temp_vehicle_0.lane_type == target_lane && temp_vehicle_0.s <= this->s && (this->s - temp_vehicle_0.s) < stip_dist)
		{
			return false;
		}
		temp_vehicle_1 = it->second[1];
		if (temp_vehicle_1.lane_type == target_lane && temp_vehicle_1.s > this->s && (temp_vehicle_1.s - this->s) < stip_dist)
		{
			return false;
		}
	}

	return true;
}

// generates predictions for non-ego vehicles to be used in trajectory generation for the ego vehicle
vector<Vehicle> Vehicle::generate_predictions(double horizon)
{
	vector<Vehicle> predictions;
	predictions.push_back(Vehicle(this->lane_type, this->x, this->y, this->s, this->d, this->yaw, this->v, this->state));

	double next_s = this->s + horizon*this->v;
	vector<double> xy = map_utils.getXY(next_s, this->d, maps_s, maps_x, maps_y);
	predictions.push_back(Vehicle(this->lane_type, xy[0], xy[1], next_s, this->d, this->yaw, this->v, this->state));

    return predictions;
}

// configure the vehicle object with the inputs provided and reset the flags
void Vehicle::configure(double targ_speed, int num_lanes, double s) 
{
    /*
    Called by simulator before simulation begins. Sets various
    parameters which will impact the ego vehicle. 
    */
    target_speed = targ_speed;
    lanes_available = num_lanes;
    goal_s = s;    
	ref_vel = 0.0;
	too_close_ahead = false;
	too_close_behind = false;
	lane_change = false;
}

// update the vehicle object with the inputs provided and reset the flags
void Vehicle::update(lane lane_type, double x, double y, double s, double d, double yaw, double v)
{
	this->lane_type = lane_type;
	this->x = x;
	this->y = y;
	this->s = s;
	this->d = d;
	this->yaw = yaw;
	this->v = v;
	too_close_ahead = false;
	too_close_behind = false;
	lane_change = false;
}

// set the member variables holding previous path values with the inputs provided
void Vehicle::set_previous_path_values(vector<double> prev_path_x, vector<double> prev_path_y, 
	double en_path_s, double en_path_d)
{
	previous_path_x.resize(prev_path_x.size());
	previous_path_x.assign(prev_path_x.begin(), prev_path_x.end());
	previous_path_y.resize(prev_path_y.size());
	previous_path_y.assign(prev_path_y.begin(), prev_path_y.end());
	end_path_s = en_path_s;
	end_path_d = en_path_d;
	prev_size = prev_path_x.size();
}

// set the member variables holding map waypoints values with the inputs provided
void Vehicle::set_map_values(const vector<double>& m_s, const vector<double>& m_x, const vector<double>& m_y)
{
	maps_s.resize(m_s.size());
	maps_s.assign(m_s.begin(), m_s.end());

	maps_x.resize(m_x.size()); 
	maps_x.assign(m_x.begin(), m_x.end());

	maps_y.resize(m_y.size());
	maps_y.assign(m_y.begin(), m_y.end());
}

// takes as input the rough trajectory finalized by the behavior planner
// generates the final smooth, drivable trajectory in global map Cartesian coordinates
vector<vector<double>> Vehicle::generate_trajectory_waypoints(trajectory traj, map<int, vector<Vehicle>> predictions)
{
	//cout << "Too close ahead: " << this->too_close_ahead << endl;
	//cout << "Too close behind:" << this->too_close_behind << endl;
	//cout << "Lane change flag:" << this->lane_change << endl;
	cout << "State (code):      " << this->state << endl;
	cout << "Final lane (code): " << traj.new_lane << endl;
	cout << "Final lane:        " << this->lane_type << endl;

	/*
	cout << "s (code):          " << traj.s << endl;
	cout << "s:                 " << this->s << endl;
	cout << "d (code):          " << traj.d << endl;
	cout << "d:                 " << this->d << endl;
	cout << "x (code):          " << traj.x << endl;
	cout << "x:                 " << this->x << endl;
	cout << "y (code):          " << traj.y << endl;
	cout << "y:                 " << this->y << endl;
	cout << "yaw (code):        " << traj.yaw << endl;
	cout << "yaw:               " << this->yaw << endl;
	*/
	
	// gradually slow down the ego vehicle if there is a vehicle in front within the stipulated distance
	// and if the speed of the ego vehicle is within 35 MPH from the target speed
	if (this->too_close_ahead && target_speed - this->ref_vel < 35)
	{
		this->ref_vel -= 0.8*0.224; // 0.224 MPH = 5 m/s2
	}
	else if (this->ref_vel < target_speed) // else gradually increase the speed of the ego vehicle upto the target speed
	{
		this->ref_vel += 1.5*0.224; // 
	}	

	// Create a list of widely spaced (x,y) waypoints, evenly spaced at 30m
	// Later, we will interpolate these waypoints with a spline and fill it in with more points that control 
	vector<double> ptsx;
	vector<double> ptsy;

	// We also want to keep track of the reference states
	// reference x,y,yaw states
	// either we will reference the starting point as where the car is or at the previous path's end point
	double ref_x = traj.x;
	double ref_y = traj.y;
	double ref_yaw = map_utils.deg2rad(traj.yaw);

	int prev_size = previous_path_x.size();
	
	if (prev_size > 0)
	{
		traj.s = end_path_s;
		this->s = end_path_s;
	}	

	// if previous size is almost empty, we use the car state as starting reference
	if (prev_size < 2)
	{
		// Use two point that make the path tangent to the car
		double prev_car_x = traj.x - cos(traj.yaw);
		double prev_car_y = traj.y - sin(traj.yaw);

		ptsx.push_back(prev_car_x);
		ptsx.push_back(traj.x);

		ptsy.push_back(prev_car_y);
		ptsy.push_back(traj.y);
	}
	// use the previous path's end point as starting reference
	else
	{
		// Basically, we are looking at the last couple of points in the previous path that the car was following,
		// and then calculating what angle the car was heading in using those last couple of points

		// Redefine reference state as previous path end point
		ref_x = previous_path_x[prev_size - 1];
		ref_y = previous_path_y[prev_size - 1];

		double ref_x_prev = previous_path_x[prev_size - 2];
		double ref_y_prev = previous_path_y[prev_size - 2];
		ref_yaw = atan2(ref_y - ref_y_prev, ref_x - ref_x_prev);

		// Use two points that make the path tangent to the previous path's end point
		ptsx.push_back(ref_x_prev);
		ptsx.push_back(ref_x);

		ptsy.push_back(ref_y_prev);
		ptsy.push_back(ref_y);
	}

	// We will push 3 more points
	// In Frenet, add evenly 30m spaced points ahead of the starting reference	
	vector<double> next_wp0 = map_utils.getXY(traj.s + stip_dist, (2 + 4 * traj.new_lane), maps_s, maps_x, maps_y);
	vector<double> next_wp1 = map_utils.getXY(traj.s + 2 * stip_dist, (2 + 4 * traj.new_lane), maps_s, maps_x, maps_y);
	vector<double> next_wp2 = map_utils.getXY(traj.s + 3 * stip_dist, (2 + 4 * traj.new_lane), maps_s, maps_x, maps_y);
	
	ptsx.push_back(next_wp0[0]);
	ptsx.push_back(next_wp1[0]);
	ptsx.push_back(next_wp2[0]);

	ptsy.push_back(next_wp0[1]);
	ptsy.push_back(next_wp1[1]);
	ptsy.push_back(next_wp2[1]);

	// The vector has 2 previous points, and the locations of the car at 30, 60, and 90 m

	// Transformation from global coordinates to local car's coordinates
	// We shift it so we make sure that the car or the last point of the previous path is at origin (0,0)
	// and it angles at 0 degree
	for (int i = 0; i < ptsx.size(); ++i)
	{
		double shift_x = ptsx[i] - ref_x;
		double shift_y = ptsy[i] - ref_y;

		// shift car reference angle to 0 degree
		ptsx[i] = (shift_x*cos(0 - ref_yaw) - shift_y * sin(0 - ref_yaw));
		ptsy[i] = (shift_x*sin(0 - ref_yaw) + shift_y * cos(0 - ref_yaw));
	}

	// create a spline
	tk::spline s;

	// set (x,y) points to the spline
	s.set_points(ptsx, ptsy);

	vector<double> next_x_vals;
	vector<double> next_y_vals;

	// Start with all of the previous path points from last time
	// This helps with the transition
	for (int i = 0; i < previous_path_x.size(); ++i)
	{
		next_x_vals.push_back(previous_path_x[i]);
		next_y_vals.push_back(previous_path_y[i]);
	}

	// Calculate how to break up spline points si that we travel at our desired reference velocity
	double target_x = stip_dist;
	double target_y = s(target_x);
	double target_dist = sqrt((target_x)*(target_x)+(target_y)*(target_y));
	
	double x_add_on = 0; // starting point

	// Fill up the rest of out path planner after filling it with previous points - here we will always output 50 points
	for (int i = 1; i <= 50 - previous_path_x.size(); ++i)
	{
		double N = (target_dist / (delta_t*this->ref_vel / mph_to_ms)); //2.24 to convert from MPH to m/s

		double x_point = x_add_on + (target_x) / N; // x_ticks
		double y_point = s(x_point);

		x_add_on = x_point;

		double x_ref = x_point;
		double y_ref = y_point;

		// rotate back to normal after rotating it earlier
		// transform back to global coordinates from local coordinates
		x_point = (x_ref*cos(ref_yaw) - y_ref*sin(ref_yaw));
		y_point = (x_ref*sin(ref_yaw) + y_ref*cos(ref_yaw));

		x_point += ref_x;
		y_point += ref_y;

		next_x_vals.push_back(x_point);
		next_y_vals.push_back(y_point);
	}

	return { next_x_vals , next_y_vals };
}

