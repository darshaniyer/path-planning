#ifndef MAP_UTILITIES_H
#define MAP_UTILITIES_H

#include <fstream>
#include <math.h>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include "json.hpp"
#include "spline.h"

using namespace std;

class Map_utilities
{
public:

	tk::spline spline_x;
	tk::spline spline_y;
	tk::spline spline_dx;
	tk::spline spline_dy;

	// Load up map values for waypoint's x,y,s and d normalized normal vectors
	vector<double> map_waypoints_x;
	vector<double> map_waypoints_y;
	vector<double> map_waypoints_s;
	vector<double> map_waypoints_dx;
	vector<double> map_waypoints_dy;

	// Load up map values for waypoint's x,y,s and d normalized normal vectors
	vector<double> new_map_waypoints_x;
	vector<double> new_map_waypoints_y;
	vector<double> new_map_waypoints_s;
	vector<double> new_map_waypoints_dx;
	vector<double> new_map_waypoints_dy;

	/**
	* Constructor
	*/
	Map_utilities();
	Map_utilities(string map_file_);

	double pi() { return M_PI; }
	double deg2rad(double x) { return x * pi() / 180; }
	double rad2deg(double x) { return x * 180 / pi(); }

	const vector<double> get_waypoints_s() { return new_map_waypoints_s; }
	const vector<double> get_waypoints_x() { return new_map_waypoints_x; }
	const vector<double> get_waypoints_y() { return new_map_waypoints_y; }

	double distance(double x1, double y1, double x2, double y2);

	int ClosestWaypoint(double x, double y, const vector<double>& maps_x, const vector<double>& maps_y);

	int NextWaypoint(double x, double y, double theta, const vector<double>& maps_x, const vector<double>& maps_y);

	vector<double> getFrenet(double x, double y, double theta, const vector<double> &maps_x, const vector<double> &maps_y);

	vector<double> getXY(double s, double d, const vector<double>& maps_s, const vector<double>& maps_x, const vector<double>& maps_y);
	   	  
	/**
	* Destructor
	*/
	virtual ~Map_utilities();
};

#endif //MAP_UTILITIES_H