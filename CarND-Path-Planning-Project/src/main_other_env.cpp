﻿#include <fstream>
#include <math.h>
#include <uWS/uWS.h>
#include <chrono>
#include <iostream>
#include <thread>
#include <vector>
#include <map>
#include <string>
#include "Eigen-3.3/Eigen/Core"
#include "Eigen-3.3/Eigen/QR"
#include "json.hpp"
#include "spline.h"
#include "Vehicle.h"
#include "map_utilities.h"

using namespace std;

// for convenience
using json = nlohmann::json;

// Checks if the SocketIO event has JSON data.
// If there is data the JSON object in string format will be returned,
// else the empty string "" will be returned.
string hasData(string s) 
{
  auto found_null = s.find("null");
  auto b1 = s.find_first_of("[");
  auto b2 = s.find_first_of("}");
  if (found_null != string::npos) 
  {
    return "";
  } 
  else if (b1 != string::npos && b2 != string::npos) 
  {
    return s.substr(b1, b2 - b1 + 2);
  }
  return "";
}

lane calc_lane(double d)
{
	lane car_lane = invalid_lane;
	if (d > 0 && d < 4)
	{
		car_lane = left_lane;
	}
	else if (d > 4 && d < 8)
	{
		car_lane = middle_lane;
	}
	else if (d > 8 && d < 12)
	{
		car_lane = right_lane;
	}

	return car_lane;
}


//all traffic in lane (besides ego) follow these speeds
vector<double> LANE_SPEEDS = { 50.0, 50.0, 50.0 };
const int num_lanes = LANE_SPEEDS.size();

// The max s value before wrapping around the track back to 0
const double goal_s = 6946.0;
const double target_speed = 49.5; // MPH
const double max_accel = 8.0; //mps2

std::string prev_ego_state = "KL";
lane prev_ego_lane = middle_lane;
int main() 
{
  uWS::Hub h;

  // Waypoint map to read from
  string map_file_ = "../data/highway_map.csv";

  // creates Map_utils object and passes in the location of the map waypoints data
  Map_utilities map_utils(map_file_);

  //creates ego vehicle object and configures it with information regarding *Δt*, 
  // initial state, target speed, number of lanes, and the goal distance in Frenet $s$ coordinate
  Vehicle ego_vehicle;
  ego_vehicle.set_state("KL");
  ego_vehicle.set_delta_t(0.02);
  ego_vehicle.set_cold_start(true);
  ego_vehicle.configure(target_speed, num_lanes, goal_s);

  // passes in the information regarding interpolated map waypoints data to the ego vehicle object
  ego_vehicle.set_map_values(map_utils.get_waypoints_s(), map_utils.get_waypoints_x(), map_utils.get_waypoints_y());

  h.onMessage([&map_utils, &ego_vehicle](uWS::WebSocket<uWS::SERVER> ws, char *data, size_t length, uWS::OpCode opCode) 
  {
    // "42" at the start of the message means there's a websocket message event.
    // The 4 signifies a websocket message
    // The 2 signifies a websocket event
    //auto sdata = string(data).substr(0, length);
    //cout << sdata << endl;
    if (length && length > 2 && data[0] == '4' && data[1] == '2') 
	{

      auto s = hasData(data);

      if (s != "") 
	  {
        auto j = json::parse(s);
        
        string event = j[0].get<string>();
        
        if (event == "telemetry") 
		{
			// Previous path data given to the Planner
			auto previous_path_x = j[1]["previous_path_x"];
			auto previous_path_y = j[1]["previous_path_y"];
			// Previous path's end s and d values 
			double end_path_s = j[1]["end_path_s"];
			double end_path_d = j[1]["end_path_d"];

			// passes in the previous path values from the telemetry data to the ego vehicle object 
			ego_vehicle.set_previous_path_values(previous_path_x, previous_path_y, end_path_s, end_path_d);

            // j[1] is the data JSON object
          
        	// receives the localization data of the ego vehicle from the telemetry, 
          	double car_x = j[1]["x"];
          	double car_y = j[1]["y"];
          	double car_s = j[1]["s"];
          	double car_d = j[1]["d"];
          	double car_yaw = j[1]["yaw"];
          	double car_speed = j[1]["speed"];
			double car_accn = 0;
			
			cout << "Distance covered: " << car_s << endl;			

			int prev_size = previous_path_x.size();

			if (prev_size > 0)
			{
				car_s = end_path_s;
			}
			

			// and updates the ego vehicle object with these data
			ego_vehicle.update(calc_lane(car_d), car_x, car_y, car_s, car_d, car_yaw, car_speed);

			// passes in the information regarding the previous state received during the last iteration 
			// from the ego vehivle to the ego vehicle
			ego_vehicle.set_state(prev_ego_state);
			int ego_key = -1;
	
			// creates a vehicle hashmap to hold vehicle data for different vehicle ids
			map<int, Vehicle> vehicles;
			// fills in the hashmap with ego vehicle data
			vehicles.insert(std::pair<int, Vehicle>(ego_key, ego_vehicle));

			//creates a prediction hashmap to hold predicted data of non - ego vehicles for different vehicle ids
			map<int, vector<Vehicle> > predictions;

          	// Sensor Fusion Data, a list of all other cars on the same side of the road.
          	auto sensor_fusion = j[1]["sensor_fusion"]; //vector<vector<double>> sensor_fusion

			// reads in sensor fusion data for each non-ego vehicle
			for (int i = 0; i < sensor_fusion.size(); ++i)
			{
				double car_id = sensor_fusion[i][0];
				double x = sensor_fusion[i][1];
				double y = sensor_fusion[i][2];
				double vx = sensor_fusion[i][3];
				double vy = sensor_fusion[i][4];
				double v = sqrt(vx*vx + vy*vy); // calculate velocity magnitude
				double s = sensor_fusion[i][5];
				double d = sensor_fusion[i][6];
				double yaw = 0;
				string state = "KL";				

				lane car_lane = calc_lane(d);

				// creates a non-ego vehicle object and populates it with current sensor fusion information, and map information
				Vehicle vehicle = Vehicle(car_lane, x, y, s, d, yaw, v, state);	
				vehicle.set_map_values(map_utils.get_waypoints_s(), map_utils.get_waypoints_x(), map_utils.get_waypoints_y());
				vehicle.set_delta_t(0.02);
				
				// update the vehicle hashmap with the information regarding non-ego vehicle
				vehicles.insert(std::pair<int, Vehicle>(car_id, vehicle));

				// calls the prediction function for the non-ego vehicle object and fills in the prediction hashmap
				double horizon = 0.02*previous_path_x.size();
				vector<Vehicle> preds = vehicle.generate_predictions(horizon);
				predictions[car_id] = preds;
			}			

			/* delete
			if (ego_vehicle.get_cold_start())
			{
				traj = ego_vehicle.keep_lane_trajectory(predictions);
				if (ego_vehicle.ref_vel >= target_speed)
				{
					ego_vehicle.set_cold_start(false);
				}
			}
			else
			{
				traj = ego_vehicle.choose_next_state(predictions);				
			}
			*/

			// call the state transition function for the ego vehicle object and receive the rough trajectory data
			trajectory traj = ego_vehicle.choose_next_state(predictions);
			
			// generate trajectory waypoints in global cartesian coordinates based on the rough trajectory data
			vector<vector<double>> trajectory_waypoints = ego_vehicle.generate_trajectory_waypoints(traj, predictions);

			// receive the final state of the ego vehicle and store it in the previous ego vehicle state for use in the next iteration
			prev_ego_state = ego_vehicle.get_state();

			if (ego_vehicle.s >= goal_s)
			{
				cout << "Successful" << endl;
			}			

			json msgJson;

			// send the trajectory waypoints data to the simulator
          	msgJson["next_x"] = trajectory_waypoints[0];
          	msgJson["next_y"] = trajectory_waypoints[1];

          	auto msg = "42[\"control\","+ msgJson.dump()+"]";

          	//this_thread::sleep_for(chrono::milliseconds(1000));
          	ws.send(msg.data(), msg.length(), uWS::OpCode::TEXT);          
        }
      } 
	  else 
	  {
        // Manual driving
        std::string msg = "42[\"manual\",{}]";
        ws.send(msg.data(), msg.length(), uWS::OpCode::TEXT);
      }
    }
  });

  // We don't need this since we're not using HTTP but if it's removed the
  // program
  // doesn't compile :-(
  h.onHttpRequest([](uWS::HttpResponse *res, uWS::HttpRequest req, char *data, size_t, size_t) 
  {
    const std::string s = "<h1>Hello world!</h1>";
    if (req.getUrl().valueLength == 1) {
      res->end(s.data(), s.length());
    } else {
      // i guess this should be done more gracefully?
      res->end(nullptr, 0);
    }
  });

  h.onConnection([&h](uWS::WebSocket<uWS::SERVER> ws, uWS::HttpRequest req) 
  {
    std::cout << "Connected!!!" << std::endl;
  });

  h.onDisconnection([&h](uWS::WebSocket<uWS::SERVER> ws, int code, char *message, size_t length) 
  {
    ws.close();
    std::cout << "Disconnected" << std::endl;
  });

  int port = 4567;
  if (h.listen("127.0.0.1", port)) 
  {
    std::cout << "Listening to port " << port << std::endl;
  } 
  else 
  {
    std::cerr << "Failed to listen to port" << std::endl;
    return -1;
  }
  h.run();
}
