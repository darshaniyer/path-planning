#include "map_utilities.h"

/**
 * Initializes Map_utilities
 */

Map_utilities::Map_utilities() {}

//ofstream resultsFile;

// receives the name of map waypoints file 
// reads in the waypoint data
// initializes the spline with the waypoint data
// uses spline to generate an interpolated set of waypoints
Map_utilities::Map_utilities(string map_file_)
{
	ifstream in_map_(map_file_.c_str(), ifstream::in);

	double max_s = 6946.0;
	string line;
	while (getline(in_map_, line))
	{
		istringstream iss(line);
		double x;
		double y;
		float s;
		float d_x;
		float d_y;
		iss >> x;
		iss >> y;
		iss >> s;
		iss >> d_x;
		iss >> d_y;
		map_waypoints_x.push_back(x);
		map_waypoints_y.push_back(y);
		map_waypoints_s.push_back(s);
		map_waypoints_dx.push_back(d_x);
		map_waypoints_dy.push_back(d_y);

		if (s > max_s)
		{
			max_s = s;
		}
	}

	spline_x.set_points(map_waypoints_s, map_waypoints_x);
	spline_y.set_points(map_waypoints_s, map_waypoints_y);
	spline_dx.set_points(map_waypoints_s, map_waypoints_dx);
	spline_dy.set_points(map_waypoints_s, map_waypoints_dy);

	//resultsFile.open("new_way_points.txt", ios::app | ios::ate);
	//resultsFile << "x" << "," << "y" << "," << "s" << "," << "dx" << "," << "dy" << "," << "\n";

	for (double s = 0; s <= floor(max_s); s+=0.5)
	{
		double x = spline_x(s);
		double y = spline_y(s);
		double dx = spline_dx(s);
		double dy = spline_dy(s);

		new_map_waypoints_s.push_back(s);
		new_map_waypoints_x.push_back(x);
		new_map_waypoints_y.push_back(y);
		new_map_waypoints_dx.push_back(dx);
		new_map_waypoints_dy.push_back(dy);

		//resultsFile << x << "," << y << "," << s << "," << dx << "," << dy << "," << "\n";
	}

	//resultsFile.close();
}

Map_utilities::~Map_utilities() {}

// calculates Euclidean distance between two points
double Map_utilities::distance(double x1, double y1, double x2, double y2)
{
	return sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));
}

// find the closest waypoint to the input x and y location using the map waypoints data
int Map_utilities::ClosestWaypoint(double x, double y, const vector<double>& maps_x, const vector<double>& maps_y)
{

	double closestLen = 100000; //large number
	int closestWaypoint = 0;

	for (int i = 0; i < maps_x.size(); i++)
	{
		double map_x = maps_x[i];
		double map_y = maps_y[i];
		double dist = distance(x, y, map_x, map_y);
		if (dist < closestLen)
		{
			closestLen = dist;
			closestWaypoint = i;
		}

	}

	return closestWaypoint;
}

// finds the next waypoint that is ahead of you, rather than the one that is closer to you but is behind you
int Map_utilities::NextWaypoint(double x, double y, double theta, const vector<double>& maps_x, const vector<double>& maps_y)
{

	int closestWaypoint = ClosestWaypoint(x, y, maps_x, maps_y);

	double map_x = maps_x[closestWaypoint];
	double map_y = maps_y[closestWaypoint];

	double heading = atan2((map_y - y), (map_x - x));

	double angle = fabs(theta - heading);
	angle = min(2 * pi() - angle, angle);

	if (angle > pi() / 4)
	{
		closestWaypoint++;
		if (closestWaypoint == maps_x.size())
		{
			closestWaypoint = 0;
		}
	}

	return closestWaypoint;
}

// transform from Cartesian x,y coordinates to Frenet s,d coordinates
vector<double> Map_utilities::getFrenet(double x, double y, double theta, const vector<double> &maps_x, const vector<double> &maps_y)
{
	int next_wp = NextWaypoint(x, y, theta, maps_x, maps_y);

	int prev_wp;
	prev_wp = next_wp - 1;
	if (next_wp == 0)
	{
		prev_wp = maps_x.size() - 1;
	}

	double n_x = maps_x[next_wp] - maps_x[prev_wp];
	double n_y = maps_y[next_wp] - maps_y[prev_wp];
	double x_x = x - maps_x[prev_wp];
	double x_y = y - maps_y[prev_wp];

	// find the projection of x onto n
	double proj_norm = (x_x*n_x + x_y * n_y) / (n_x*n_x + n_y * n_y);
	double proj_x = proj_norm * n_x;
	double proj_y = proj_norm * n_y;

	double frenet_d = distance(x_x, x_y, proj_x, proj_y);

	//see if d value is positive or negative by comparing it to a center point

	double center_x = 1000 - maps_x[prev_wp];
	double center_y = 2000 - maps_y[prev_wp];
	double centerToPos = distance(center_x, center_y, x_x, x_y);
	double centerToRef = distance(center_x, center_y, proj_x, proj_y);

	if (centerToPos <= centerToRef)
	{
		frenet_d *= -1;
	}

	// calculate s value
	double frenet_s = 0;
	for (int i = 0; i < prev_wp; i++)
	{
		frenet_s += distance(maps_x[i], maps_y[i], maps_x[i + 1], maps_y[i + 1]);
	}

	frenet_s += distance(0, 0, proj_x, proj_y);

	return { frenet_s,frenet_d };
}

// transform from Frenet s,d coordinates to Cartesian x,y
vector<double> Map_utilities::getXY(double s, double d, const vector<double>& maps_s, const vector<double>& maps_x, const vector<double>& maps_y)
{
	int prev_wp = -1;

	while (s > maps_s[prev_wp + 1] && (prev_wp < (int)(maps_s.size() - 1)))
	{
		prev_wp++;
	}

	int wp2 = (prev_wp + 1) % maps_x.size();

	double heading = atan2((maps_y[wp2] - maps_y[prev_wp]), (maps_x[wp2] - maps_x[prev_wp]));
	// the x,y,s along the segment
	double seg_s = (s - maps_s[prev_wp]);

	double seg_x = maps_x[prev_wp] + seg_s * cos(heading);
	double seg_y = maps_y[prev_wp] + seg_s * sin(heading);

	double perp_heading = heading - pi() / 2;

	double x = seg_x + d * cos(perp_heading);
	double y = seg_y + d * sin(perp_heading);

	return { x,y };
}

