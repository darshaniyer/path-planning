#include <functional>
#include <iterator>
#include <map>
#include <math.h>
#include "cost.h"


// Set weights for cost functions.
const double REACH_GOAL = 150;
const double EFFICIENCY = 0;
const double LANE_CHANGE = 75;

// the metric rewards state that takes us closer to our goal of 6946m. 
double goal_distance_cost(const Vehicle& vehicle, const trajectory& traj, 
	const map<int,vector<Vehicle>>& predictions, map<string, double>& data)
{
	double goal_s = vehicle.goal_s;
	double distance_to_goal = data["distance_to_goal"];

	double cost = distance_to_goal / goal_s;
	//double cost = static_cast<double>((int)distance_to_goal % (int)goal_s) / goal_s;

	return cost;	   	 	
}

// the metric favors agressive driving and frequent lane changes as it rewards keeping speed closer to the target speed
double inefficiency_cost(const Vehicle& vehicle, const trajectory& traj,
	const map<int, vector<Vehicle>>& predictions, map<string, double>& data) 
{
	lane intended_lane = static_cast<lane>((int)data["intended_lane"]);
    double proposed_speed_intended = lane_speed(predictions, intended_lane, data["target_speed"]);
    //if no vehicle is in the proposed lane, we can travel at target speed.
    if (proposed_speed_intended < 0) 
    {
        proposed_speed_intended = vehicle.target_speed;
    }

	lane final_lane = static_cast<lane>((int)data["final_lane"]);
    double proposed_speed_final = lane_speed(predictions, final_lane, data["target_speed"]);
	if (proposed_speed_final < 0)
	{
		proposed_speed_final = vehicle.target_speed;
	}
    
    double cost = (2.0*vehicle.target_speed - proposed_speed_intended - proposed_speed_final)/vehicle.target_speed;

    return cost;
}

// find the speed of the fastest vehicle in the input lane
double lane_speed(const map<int, vector<Vehicle>>& predictions, lane lane_type, double target_speed)
{
	double v_speed = -1.0;
	
    for (map<int, vector<Vehicle>>::const_iterator it = predictions.begin(); it != predictions.end(); ++it) 
    {
        int key = it->first;
        Vehicle vehicle = it->second[0];
        if (vehicle.lane_type == lane_type && key != -1)
        {
			if (vehicle.v > v_speed)
			{
				v_speed = vehicle.v;
			}
        }
    }

	if (v_speed > target_speed)
	{
		v_speed = target_speed;
	}
	
    return v_speed;	
}

// binary cost function calculated based on three flags
// too close flag - 1 if there is non - ego vehicle in the lane of the ego vehicle within 30m in front
// too close behind - 1 if there is non - ego vehicle in the lane of the ego vehicle within 15m behind the ego vehicle
// lane change - 1 if lane change is possible
double lane_change_cost(bool too_close_ahead, bool too_close_behind, bool lane_change)
{
	double cost = 1.0;
		
	if (too_close_ahead && !too_close_behind && lane_change)
	{
		cost = 0.0;
	}	
	
	return cost;		
}

// calculates the overall cost
double calculate_cost(const Vehicle& vehicle, const map<int, vector<Vehicle>>& predictions, const trajectory& traj,
	bool too_close_ahead, bool too_close_behind, bool lane_change)
{
    map<string, double> trajectory_data = get_helper_data(vehicle, traj, predictions);
    double cost = 0.0;

	// sum weighted cost functions to get total cost for trajectory.
	cost += REACH_GOAL * goal_distance_cost(vehicle, traj, predictions, trajectory_data);
	cost += EFFICIENCY * inefficiency_cost(vehicle, traj, predictions, trajectory_data);
	cost += LANE_CHANGE * lane_change_cost(too_close_ahead, too_close_behind, lane_change);

    return cost;
}

// generates helper data to use in cost functions
map<string, double> get_helper_data(const Vehicle& vehicle, const trajectory& traj, const map<int, vector<Vehicle>> & predictions)
{
    // indended_lane: +/- 1 from the current lane if the vehicle is planning or executing a lane change.
    // final_lane: The lane of the vehicle at the end of the trajectory. The lane is unchanged for KL and PLCL/PLCR trajectories.
    // distance_to_goal: The s distance of the vehicle to the goal.
	// target_speed: target speed of the vehicle

    map<string, double> trajectory_data;    

    trajectory_data["intended_lane"] = static_cast<double>(traj.new_lane); 
    trajectory_data["final_lane"] = static_cast<double>(traj.new_lane);
    trajectory_data["distance_to_goal"] = vehicle.goal_s - traj.s;
	trajectory_data["target_speed"] = vehicle.target_speed;

    return trajectory_data;
}