#ifndef COST_H
#define COST_H

#include "vehicle.h"

using namespace std;

double calculate_cost(const Vehicle& vehicle, const map<int, vector<Vehicle>>& predictions, const trajectory& traj,
	bool too_close_ahead, bool too_close_behind, bool lane_change);

double lane_change_cost(bool too_close_ahead, bool too_close_behind, bool lane_change);

double goal_distance_cost(const Vehicle& vehicle, const trajectory& traj,  
	const map<int, vector<Vehicle>>& predictions, map<string, double>& data);

double inefficiency_cost(const Vehicle& vehicle, const trajectory& traj, 
	const map<int, vector<Vehicle>> & predictions, map<string, double>& data);

double lane_speed(const map<int, vector<Vehicle>>& predictions, lane lane_type, double target_speed);

map<string, double> get_helper_data(const Vehicle& vehicle, const trajectory& traj, 
	const map<int, vector<Vehicle>>& predictions);

#endif