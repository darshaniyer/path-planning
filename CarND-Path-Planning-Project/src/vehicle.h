#ifndef VEHICLE_H
#define VEHICLE_H
#include <iostream>
#include <random>
#include <vector>
#include <map>
#include <string>
#include "spline.h"
#include "map_utilities.h"

using namespace std;

enum lane { left_lane, middle_lane, right_lane, invalid_lane };
struct trajectory
{
	double s;
	double d;
	double x;
	double y;
	double yaw;
	lane current_lane;
	lane new_lane;
	std::string state;
	bool is_exist;
};

const double mph_to_ms = 2.24;
const double stip_dist = 30.0;

class Vehicle 
{
public:

  map<std::string, int> lane_direction = {{"LCL", -1}, {"LCR", 1}};

  bool cold_start;

  bool too_close_ahead;
  bool too_close_behind;
  bool lane_change;
  
  double delta_t;

  lane lane_type;

  double x;

  double y;

  double yaw;

  double s;

  double d;

  double v;

  double target_speed;

  double ref_vel;

  int lanes_available;

  double goal_s;

  std::string state;

  vector<double> previous_path_x;
  vector<double> previous_path_y;
  double end_path_s;
  double end_path_d;
  int prev_size;

  vector<double> maps_s;
  vector<double> maps_x;
  vector<double> maps_y;

  Map_utilities map_utils;

  /**
  * Constructor
  */
  Vehicle();
  Vehicle(lane lane_type, double x, double y, double s, double d, double yaw, double v, std::string state="CS");

  void update(lane lane_type, double x, double y, double s, double d, double yaw, double v);

  void set_delta_t(double dt) { delta_t = dt; }  

  void set_state(std::string state) { this->state = state;}

  std::string get_state() { return this->state; }

  void set_cold_start(bool cold_start) { this->cold_start = cold_start; }

  bool get_cold_start() { return cold_start; }

  void set_previous_path_values(vector<double> previous_path_x, vector<double> previous_path_y, double end_path_s, double end_path_d);

  void set_map_values(const vector<double>& maps_s, const vector<double>& maps_x, const vector<double>& maps_y);
  
  /**
  * Destructor
  */
  virtual ~Vehicle();

  trajectory choose_next_state(map<int, vector<Vehicle>> predictions);

  vector<std::string> successor_states();

  trajectory generate_trajectory(std::string state, map<int, vector<Vehicle>> predictions);

  trajectory keep_lane_trajectory(map<int, vector<Vehicle>> predictions);

  trajectory lane_change_trajectory(std::string state, map<int, vector<Vehicle>> predictions);  

  bool is_vehicle_behind(map<int, vector<Vehicle>> predictions, lane lane_type);

  bool is_vehicle_ahead(map<int, vector<Vehicle>> predictions, lane lane_type);

  bool is_lane_change_possible(map<int, vector<Vehicle>> predictions, lane lane_type);

  vector<Vehicle> generate_predictions(double horizon=30.0);

  void configure(double target_speed, int num_lanes, double goal_s);

  vector<vector<double>> generate_trajectory_waypoints(trajectory traj, map<int, vector<Vehicle>> predictions);
};

#endif