## Path planning

Path planner is like the brain of an autonomous vehicle (AV) - it is how a vehicle decides where to go and how to get there. There are actually three parts to path planning - prediction, behavior planning, and trajectory generation. Figure 1 depicts the overall interaction between different planning modules and the corresponding flow of data.

[image1]: ./figures/Path_planner.jpg "Path planner"
![alt text][image1]
**Figure 1 Path planner: different modules and flow of data. The clock on the left indicates the time spent in each module.**

*Prediction* involves predicting what other vehicles in the road might do next; the prediction module uses information from the map of the world and data from sensor fusion, and generate as output some predictions of the future state of all the vehicles and other moving objects in the vicinity of the ego vehicle. *Behavior planner* decides what maneuver to perform next at a higher level such as keep lane, change lane, etc., taking into account the estimates from prediction, and localization data of the ego vehicle. *Trajectory generation* takes as input the localization data of the ego vehicle, the prediction of the behavior of other cars from the prediction module, and the maneuver decision from behavior planner to generate a smooth, safe, and drivable trajectory for the ego vehicle. The trajectory is not just a curve in _x_ and _y_, but it also has a time dimension because we have to set the speed of the vehicle at the same time. This trajectory is input to the controller module that executes the trajectory.

The goal of this project is to implement a path planning algorithm to navigate a car around a simulated highway scenario with other traffic that is driving $`\pm`$10 MPH of the 50 MPH speed limit, given map waypoints, telemetry (ego car localization data), and sensor fusion data of other vehicles. The car must not violate a set of motion constraints, namely maximum velocity of 50 MPH, maximum acceleration of 10 m/$`s^2`$, and maximum jerk of 10 m/$`s^3`$, while also avoiding collisions with other vehicles, keeping to within a highway lane (aside from short periods of time while changing lanes), and changing lanes when doing so is necessary to maintain a speed near the posted speed limit. The car should be able to make one complete loop around the 6946m highway. 

## Overall steps

The path planning algorithm consists of the following steps:

* Generate interpolated waypoints of highway map
* Generate predictions of non-ego vehicles
* Decide a maneuver based on finite state machine in behavior planning
    * Generate rough trajectory for every state in behavior planner
    * Compute cost of each trajectory that accounts for safety and efficiency
    * Select the trajectory with the lowest cost
* Convert the trajectory from Frenet (_s_, _d_) coordinates to global cartesian (_x_, _y_) map coordinates.

## Repository

The original Udacity project instructions can be found [here](https://github.com/udacity/CarND-Path-Planning-Project).

The Udacity simulator required for this project can be downloaded from [here](https://github.com/udacity/self-driving-car-sim/releases/tag/T3_v1.2).

## Code base

The project was implemented in Visual Studio in Windows 10.

1. In the CarND-Path-Planning-Project, open the path_planner.sln file.
2. Rebuild Solution.
3. Open the simulator.
4. Click F5 in Visual Studio to debug. This attaches the code to the simulator.
5. Click "Start" on the simulator.

To compile the code in Linux or Mac OS or Docker environment, 
1. Go to CarND-Path-Planning-Project\src folder
1. Delete main.cpp 
2. Rename main_other_env.cpp as main.cpp.

## Detailed writeup

Detailed report can be found in [_Path_planning_writeup.md_](Path_planning_writeup.md).

## Results

Below are the snapshots of the car driving in the simulator at different stages. 

[image8]: ./figures/Results_1.jpg "At 57 seconds"
![alt text][image8]
**Figure 8 Snapshot at 57 seconds**

[image9]: ./figures/Results_2.jpg "At 3:13 minutes"
![alt text][image9]
**Figure 9 Snapshot at 3:13 minutes**

[image10]: ./figures/Results_3.jpg "At 5:14 minutes"
![alt text][image10]
**Figure 10 Snapshot at 5:14 minutes**

Please find the video of the simulator for the first mile.

![](./videos/Path_planning_video.mp4)

## Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. Special thanks to the author of the [blog post]( http://www.codza.com/blog/udacity-uws-in-visualstudio) that helped me setup the Visual studio environment for this project. The idea of interpolation of waypoints was inspired by the work of Philippe Weingertner. The project walkthrough by Aaron Brown and David Silver was extremely helpful.

