
## Path planning

Path planner is like the brain of an autonomous vehicle (AV) - it is how a vehicle decides where to go and how to get there. There are actually three parts to path planning - prediction, behavior planning, and trajectory generation. Figure 1 depicts the overall interaction between different planning modules and the corresponding flow of data.

[image1]: ./figures/Path_planner.jpg "Path planner"
![alt text][image1]
**Figure 1 Path planner: different modules and flow of data. The clock on the left indicates the time spent in each module.**

*Prediction* involves predicting what other vehicles in the road might do next; the prediction module uses information from the map of the world and data from sensor fusion, and generate as output some predictions of the future state of all the vehicles and other moving objects in the vicinity of the ego vehicle. *Behavior planner* decides what maneuver to perform next at a higher level such as keep lane, change lane, etc., taking into account the estimates from prediction, and localization data of the ego vehicle. *Trajectory generation* takes as input the localization data of the ego vehicle, the prediction of the behavior of other cars from the prediction module, and the maneuver decision from behavior planner to generate a smooth, safe, and drivable trajectory for the ego vehicle. The trajectory is not just a curve in $x$ and $y$, but it also has a time dimension because we have to set the speed of the vehicle at the same time. This trajectory is input to the controller module that executes the trajectory.

The goal of this project is to implement a path planning algorithm to navigate a car around a simulated highway scenario with other traffic that is driving $\pm$10 MPH of the 50 MPH speed limit, given map waypoints, telemetry (ego car localization data), and sensor fusion data of other vehicles. The car must not violate a set of motion constraints, namely maximum velocity of 50 MPH, maximum acceleration of 10 m/$s^2$, and maximum jerk of 10 m/$s^3$, while also avoiding collisions with other vehicles, keeping to within a highway lane (aside from short periods of time while changing lanes), and changing lanes when doing so is necessary to maintain a speed near the posted speed limit. The car should be able to make one complete loop around the 6946m highway. 

The simulator returns instantaneous telemetry data for the ego vehicle, but it also returns the list of points from previously generated path. 

As an output, the simulator expects a set of points ($x$, $y$) in global cartesian map coordinates, spaced in time at 0.02 seconds representing the car's trajectory. The uWebSockets implementation of web-socket handles the communication between the simulator and the path planner. 

## Assumptions 

Every 20ms, the car moves to the next point on the trajectory. The velocity of the car depends on the spacing of the points. Because the car moves to a new waypoint every 20ms, the larger the spacing between the points, the faster the car will travel. Acceleration is calculated by comparing the rate of change of average speed over 20ms intervals. Jerk is calculated by comparing the rate of change of average acceleration over 1s intervals. Part of the total acceleration is the normal component, $AccN$, which measures the centripetal acceleration from turning. The tighter and faster a turn is made, the higher the $AccN$ will be. In case we do not turn, $AccN$ will be 0.


##  Overall steps 

The path planning algorithm consists of the following steps:

* Generate interpolated waypoints of highway map
* Generate predictions of non-ego vehicles
* Decide a high level maneuver based on finite state machine implementation of behavior planner
    * Generate rough trajectory for every state 
    * Compute cost of each trajectory that accounts for parameters like goal-orienation, safety, and efficiency
    * Select the trajectory with the lowest cost
* Generate a smooth, drivable, and safe trajectory in Frenet ($s$, $d$) coordinates
* Convert the trajectory from Frenet ($s$, $d$) coordinates to global cartesian ($x$, $y$) map coordinates.

###  Frenet coordinates

Frenet coordinates is a way of representing position on a road in a more intuitive way than traditional ($x$, $y$) Cartesian coordinates. With Frenet coordinates, we use the variables $s$ and $d$ to describe the position of vehicle on the road, where $s$ coordinate represents distance along the road, also known as longitudinal displacement, and $d$ coordinate represents side-to-side position on the road, known as lateral displacement.

[image2]: ./figures/Frenet_coordinates.jpg "Frenet coordinates"
![alt text][image2]
**Figure 2 Frenet_coordinates**

As shown in Figure 2, imagine a curvy road with the Cartesian coordinate system laid on top of it. Using these Cartesian coordinates, we can try to describe the path a vehicle would normally follow on the road. Notice how curvy that path is. The mathematical equations describing this path in terms of Cartesian coordinates would be quite involved. Using the Frenet coordinate system, we have $s$=0 to represent the beginning of the segment of road and $d$=0 to represent the center line of that road. To the left of the center line, we have -$d$ and to the right, we have +$d$. If the vehicle were moving at a constant speed of $v_{0}$ in the center line, we could write a mathematical description of the vehicle's position as $s(t)$ = $v_{0}.t$ and $d(t)$ = 0. 


####  The need for time 

It is important not to just compute a sequence of configurations, but also to decide when are we going to be in each configuation. The problem of not incorporating time into our planning will render it incomplete. There is traffic on highway, which means the traversable space is constantly changing over time. To avoid collision in dynamic environment, we need to think about time as well. Thus, driving is fundamentally a 3D problem. Incorporating the time component converts 2D path into a 3D trajectory that describes the motion of the vehicle over time. 

###  Map data 

For different maneuvers, it is much more convenient to think in terms of Frenet ($s$, $d$) coordinates rather than in Cartesian ($x$, $y$) coordinates. So a key point in the implementation is being able to do accurate ($x$, $y$) -> ($s$, $d$) -> ($x$, $y$) transformations, for which map data are used.

The map of the highway is given in terms of list of waypoints. Each waypoint in the list contains [$x, y, s, dx, dy$] values. $x$ and $y$ are the waypoint's map coordinate position, the $s$ value is the distance along the road to get to that waypoint in meters, the $dx$ and $dy$ values define the unit normal vector pointing outward of the highway loop. The highway's waypoints loop around so the frenet $s$ value, distance along the road, goes from 0 to 6945.554m. The map is provided as a discrete set of 181 points spaced roughly 30m apart. In addition, there is a large discontinuity after the last point, which can be clearly seen in Figure 3. The first step in the process is to use Splines to interpolate the track in between these points and produce a set of much more tightly spaced (0.5m apart) waypoints which help to produce more accurate results from the coordinate transform methods, and also account for the discontinuity in $s$ values at the end/beginning of the track. Figure 3 illustrates the waypoints data before and after the interpolation process.

[image3]: ./figures/Map_waypoints.jpg "Map waypoints"
![alt text][image3]
**Figure 3 Map waypoints with (blue) and without interpolation (red)**

###  Prediction 

We use data from sensor fusion and waypoints data from map to predict the positions of other vehicles in future. The motion model we use is a linear, constant velocity model. The car is modeled as a point particle with holonomic (can instantly move in any direction) properties, and it moves forward each time step and is assumed to keep a constant distance to the lane center. 

Sensor fusion data consists of a list of attributes of all the non-ego vehicles on the same side of the road as the ego vehicle. A 2D vector consisting of cars and their attributes - unique ID, $x$ position in map coordinates, $y$ position in map coordinates, $x$ velocity in m/s, $y$ velocity in m/s,  $s$ position in Frenet coordinates, $d$ position in Frenet coordinates.

In this project, the prediction holds two pieces of information for each non-ego vehicle: the state of the vehicle at current time, and the state of the vehicle in terms of the Frenet coordinate $s$-position at future time instant calculated using its current velocity, the *Δt* of 0.02s, and the size of the previous path. The vehicle is assumed to keep the lane.

The prediction data is used to ascertain if there is any vehicle within a stipulated distance of 30m:
* in front of the ego vehicle in the same lane in future, 
* behind the ego vehicle in the same lane at current time,
* in front of the ego vehicle in the target lane in future, 
* behind the ego vehicle in the target lane at current time. 

###  Behavior planning 

In behavior planning, we address the problem of what to do next at macroscopic level such as should we keep lane or change lane, and not the microscopic decision of what exact control input to give to a steering wheel. The behavior planning module is responsible for providing guidance to the trajectory planner about sorts of maneuver it should plan trajectories for. 

As depicted in Figure 4, the behavior planner  takes as input: map of the world, route to the destination, and predictions about what other static and dynamic obstacles are likely to do; it produces as output a suggested maneuver for the ego vehicle, which the trajectory planner is responsible for generating. The responsibilites of behavior planner module are to suggest maneuvers which are feasible, safe, legal, and efficient. It is not responsible for execution details and collision avoidance.

[image4]: ./figures/Behavior_planning_overview.jpg "Behavior planning overview.png"
![alt text][image4]
**Figure 4 Behavior planning overview**

As depicted in Figure 5, it is the slowest because it has to incorporate lots of data to make decisions about fairly long time horizon in order of 10s or even more.

[image5]: ./figures/Timing.jpg "Timing"
![alt text][image5]
**Figure 5 Timing**

####  Finite state machines (FSM) 

FSMs are used to solve the behavior planning problem. A FSM makes decision based on finite set of discrete states. When initialized, a FSM begins in some start state. Any state can be connected by one or more transitions; sometimes, there is a transition back to the same state called self transition. Not all transitions are necessarily possible. A state that does not transition to any other state is called accepting state. For non-accepting states, there can often be multiple potential successory states. The FSM uses state transition function to decide which state to go next from its current state.

[image6]: ./figures/FSM.jpg "FSM for our behavioral planner"
![alt text][image6]
**Figure 6 FSM for our behavioral planner**

Figure 6 depicts the FSM used in our behavioral planner module. It consists of 3 states: keep lane (KL), lane change left (LCL), and lane change right (LCR). 

######  Keep lane 

* d - stay near the center line for lane (In Frenet coordinates, target $d$ for the vehicle is whatever the $d$ for the lane is)
* s - drive at target speed when feasible, otherwise, drive at safe speed

The state includes speeding up or slowing down behavior.

##### Lane change left / right 

The goal is to move from initial lane to the target lane.

* d - move left or right as appropriate. The target $d$ is the $d$ for whatever lane is to the left or right of the ego vehicle's current lane
* s - same rules as keep lane for the initial lane. The vehicle will try to drive at the target speed, but if not possible, drive at the safe speed for the initial lane.

####  Transition functions 

The state we choose impact the driving behavior of the vehicle, but to decide how the state transitions, a transition function is used. The transition function consists of following four steps: 
* get the successor states for the current state
* generate a rough trajectory for each state 
* calculate cost of that trajectory 
* find the state and the corresponding rough trajectory with the lowest cost.

####  Cost functions 

A key part of getting transitions to happen when we want them to is to design reasonable cost functions so that we are able to penalize the wrong and reward the right behavior. We incorporated following cost functions:

* Goal distance cost: $\frac{goal_{dist} - current_{dist}}{goal_{dist}}$. The metric rewards the state that takes us closer to our goal of 6946m. 
* Inefficiency cost: $\frac{speed_{target} - speed_{target_{lane}}}{speed_{target}} - \frac{speed_{target} - speed_{intended_{lane}}}{speed_{target}}$. This metric favors agressive driving and frequent lane changes as it rewards keeping speed closer to the target speed.
* Lane change cost: Binary cost function calculated based on three flags
    * too close flag - 1 if there is non-ego vehicle in the lane of the ego vehicle within 30m in front
    * too close behind - 1 if there is non-ego vehicle in the lane of the ego vehicle within 30m behind the ego vehicle
    * lane change - 1 if lane change is possible.
    
Lane change cost is 1 even if one of the above three flags is not 1, and is 0 if all the above three flags are 1. This cost ensures safety and collision-free lane changes.

The driving policy is typically defined by these cost functions. It can be tuned to have a very conservative driving experience by keeping a rather safe distance with the vehicle in front of us and perform lane changes only when there is sufficient free space in the target lane, or it can be tuned to target a more speedy driving experience, by making frequent lane changes so long one can drive close to the target speed. The tricky part is balancing different costs, since often our goals conflict. We combine all the cost functions into one weighted cost function, and use it to calculate the cost of the trajectory. A delicate choice of weights for these cost functions result in trajectory that is goal oriented, efficient, and safe. We chose weight values of 150, 0, and 75 as we went for goal oriented and safe planner, that executes lane changes only when can be executed safely.


###  Trajectory generation

After behavior selection, we go a level lower, where we actually want to find a specific smooth, safe, legal, and drivable trajectory that the car can take. A trajectory is not just a curve that the car can follow, but also a time sequence in which we say how fast the car should go. In finding trajectory, there are many important things to watch out for, such as collision-freeness, passenger comfort, etc. We do not want trajectory that goes back and forth; the trajectory should be as smooth and elegant as possible. 

Search algorithms such as A$*$, hybrid A$*$ are best suited for unstructured environments like parking lot or maze, which tend to have less specific rules (than structured environments like highway or streets), lower speed limits, and no obvious reference paths or trajectory that corresponds to what we should be doing 90% of the time, as they change so much. On the other hand, highway or streets are highly structured environments, where all motions are constrained by predefined rules regarding how we can move on the road, e.g., the direction of the traffic, lane boundaries, speed limits, etc. All these rules impose constraints which have to be satisfied, but also provide guidance as to how a trajectory should look like. For such structured environments, sampling-based methods such as polynomial-based trajectory generation methods are optimal. These methods generate continuous trajectories which satisfy different boundary conditions. 

For example, let us say we are getting off the highway. So we want to change the lane in order to end up there after some *Δt*, say 10s. The start position and the goal position are fixed, and they define the boundary conditions of our trajectory at $t$=0 and $t$=10. If these were the only boundary conditions, then we might consider joining the two positions with a straight line. Unfortunately, the kink in the slope would translate in an instantaneous jump in speed, which would require infinite acceleration. If we were to send this to our motion control module, it would result in a very high acceleration of our car, which is both uncomfortable and dangerous. This is why we need continuity and smoothness in our trajectories, for which we need continuity in position, velocity, acceleration, and jerk, which is directly related to human perception of comfort; we can tolerate high acceleration, but we do not like when our acceleration changes too quickly, which is high jerk. This is why polynomial-based methods for trajectory generation have to be jerk optimal.

In this project, we generate smooth, drivable trajectory based on a polynomial method using the [Spline library](https://kluge.in-chemnitz.de/opensource/spline/). The trajectory is 50 samples in size, and it takes the car 0.02s to travel between each pair of consecutive points. 

First, a set of five anchor points are used to initialize the spline calculation - the last two points of the previous trajectory (or the car position if there is no previous trajectory), and three points that are spaced 30m, 60m, and 90m, respectively from the starting position. To facilitate simplicity in calculation, these anchor points are transformed from global cartesian coordinates to local vehicle coordinates. This transformation is achieved by shifting each anchor point to the origin (0,0), and then rotating the shifted anchor point such that it angles at 0 degree using the reference position and angle of the car's current path or the last point of the previous path.

In order to ensure continuity and smooth transition in the trajectory (in addition to adding the last two point of the past trajectory to the spline adjustment), the past trajectory points (in global cartesian coordinates) are copied to the new trajectory. The balance number of points in the trajectory are filled with spline function that was fitted with the anchor points. As shown in Figure 7, we take into account the target goal of 30m from the current position, sampling period *Δt* of 0.02s, and the desired reference velocity to split the spline into N pieces. 

[image7]: ./figures/Trajectory_calculation.jpg "Trajectory calculation using spline"
![alt text][image7]
**Figure 7 Trajectory calculation using spline. s(30) is value of spline function at 30.**

Finally, the trajectory points in vehicle coordinate system are transformed back to global coordinate system by first rotating each point with respect to reference yaw angle, and then translating with respect to reference position. 




## File structure 

Following are the project files in the *src* folder:

* *main.cpp* - 
  * main()  
    - creates Map_utils object and passes in the location of the map waypoints data 
    - creates ego vehicle object and configures it with information regarding *Δt*, initial state, target speed, number of lanes, and the goal distance in Frenet $s$ coordinate
    - passes in the information regarding interpolated map waypoints data to the ego vehicle object
    - passes in the previous path values from the telemetry data to the ego vehicle object
    - receives the current localization data of the ego vehicle from the telemetry, and updates the ego vehicle object with these data
    - passes in the information regarding the previous state (received during the last iteration from the ego vehicle) to the ego vehicle
    - creates a vehicle hashmap to hold vehicle data for different vehicle ids
    - fills in the hashmap with ego vehicle data 
    - creates a prediction hashmap to hold prediction data of non-ego vehicles for different vehicle ids
    - reads in sensor fusion data for each non-ego vehicle
    - creates a non-ego vehicle object and populates it with current sensor fusion information and map information
    - updates the vehicle hashmap with the information regarding non-ego vehicle
    - calls the prediction function for the non-ego vehicle object and fills in the prediction hashmap
    - calls the state transition function for the ego vehicle object, and receives the rough trajectory data
    - generates trajectory waypoints in global cartesian coordinates based on the rough trajectory data
    - sends the trajectory waypoints data to the simulator
    - receives the final state of the ego vehicle, and stores it in the previous ego vehicle state for use in the next iteration
* *map_utilities.cpp* - 
  * Map_utilities()
    - receives the name of map waypoints file 
    - reads in the waypoint data
    - initializes the spline with the waypoint data
    - uses spline to generate an interpolated set of waypoints
  * ClosestWaypoint() - finds the closest waypoint to the input $x$ and $y$ location using the map waypoints data
  * NextWaypoint() - finds the next waypoint that is ahead of you, rather than the one that is closer to you but is behind you
  * distance() - calculates Euclidean distance between two points
  * getFrenet() - transforms from Cartesian $x$,$y$ coordinates to Frenet $s$,$d$ coordinates using map waypoints data
  * getXY() - transform from Frenet $s$,$d$ coordinates to Cartesian $x$,$y$ coordinates using map waypoints data
* *vehicle.cpp* - 
  * Vehicle() - construct a Vehicle object and initialize it with the provided arguments 
  * choose_next_state() - calls
     - successor_states() - uses the current state to return a vector of possible successor states for the finite state machine
     - is_vehicle_ahead() - checks if there is any vehicle in front of the ego vehicle
     - is_vehicle_behind() - checks if there is any vehicle behind the ego vehicle
     - generate_trajectory() - returns a trajectory `struct` representing a rough vehicle trajectory, given the state and the predictions. Note that `is_exist` element is false if no possible trajectory exists for the state.
  * calculate_cost() - computes the cost for the above trajectory
  * keep_lane_trajectory() - generates a keep lane trajectory
  * lane_change_trajectory() - generates a lane change trajectory
  * is_lane_change_possible() - checks if lane change is possible
  * configure() - configures the vehicle object with the inputs provided, and resets the flags
  * update() - updates the vehicle object with the inputs provided, and resets the flags
  * set_previous_path_values() - sets the member variables holding previous path values with the inputs provided
  * set_map_values() - sets the member variables holding map waypoints values with the inputs provided
  * generate_trajectory_waypoints() - 
     - takes as input the rough trajectory finalized by the behavior planner
     - generates a final smooth, drivable trajectory in global map Cartesian coordinates
* *cost.cpp* - 
  * goal_distance_cost() - the metric rewards state that takes us closer to our goal of 6946m
  * inefficiency_cost() - the metric favors aggressive driving and frequent lane changes as it rewards keeping speed closer to the target speed
  * lane_speed() - finds the speed of the fastest vehicle in the input lane
  * lane_change_cost() - binary cost function calculated based on three flags 
     - too close flag - 1 if there is non-ego vehicle in the lane of the ego vehicle within 30m in front
     - too close behind - 1 if there is non-ego vehicle in the lane of the ego vehicle within 15m behind the ego vehicle
     - lane change - 1 if lane change is possible
  * calculate_cost() - calculates the overall cost
  * get_helper_data() - generates helper data to use in cost functions. 

The Term 3 simulator is a client, and the C++ program software is a web server. *main.cpp* receives the telemetry localization data for the ego vehicle, sensor fusion data for the non-ego vehicles, and the previous path values from the simulator, and provides the $x$ and $y$ coordinate values of the trajectory in global map coordinate system to the simulator. *main.cpp* is made up of several functions within main(), these all handle the uWebsocketIO communication between the simulator and it's self.


## Results 

Below are the snapshots of the car driving in the simulator at different stages. 

[image8]: ./figures/Results_1.jpg "At 57 seconds"
![alt text][image8]
**Figure 8 Snapshot at 57 seconds**

[image9]: ./figures/Results_2.jpg "At 3:13 minutes"
![alt text][image9]
**Figure 9 Snapshot at 3:13 minutes**

[image10]: ./figures/Results_3.jpg "At 5:14 minutes"
![alt text][image10]
**Figure 10 Snapshot at 5:14 minutes**





```python
# Please find the video of the simulator for the first mile.

# <video controls src="videos/Path_planning_video.mp4" />
```

### Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. Special thanks to the author of the blog post that helped me setup the Visual studio environment for this project - http://www.codza.com/blog/udacity-uws-in-visualstudio. The idea of interpolating of waypoints were based on the work of Philippe Weingertner. The project walkthrough video by Aaron Brown and David Silver was extremely helpful.

